if(sessionStorage.getItem("id") == null || sessionStorage.getItem("id") == 'undefined') {
	window.location.replace("login.html");
};
$(function() {
	$("#toSearch").click(function() {
		window.location.href = "search.html";
	});
	$("#toFavorites").click(function() {
		window.location.href = "favorites.html";
	});
	$("#toHistory").click(function() {
		window.location.href = "history.html";
	})
	$("#logOutButton").click(function() {
		sessionStorage.setItem("id", null);
	});
})