$(function() {		
	$("#submitLogin").click(function() {
	var info = {
		username: $("#usernameField").val(),
		password: $("#passwordField").val()
	}
	
	$.ajax({
		type: 'POST',
		url: 'http://localhost:8080/WeatherServer/LoginServlet',
		data: info,
		success: function (data) {
			var loginInfo = $.parseJSON(data);
			sessionStorage.setItem("id", loginInfo.id);
			window.location.href = "main.html";
		},
		error: function () {
		}
	})	
	
});
	
$("#submitRegister").click(function() {
	
	var info = {
		username: $("#regUsernameField").val(),
		password: $("#regPasswordField").val(), // Still back-end sucks
		firstname: $("#regFirstNameField").val(),
		lastname: $("#regLastNameField").val(),
		email: $("#regEmailField").val(),
	}
	if (info.password == $("#regSecondPasswordField").val()) {
		$.ajax({
			type: 'POST',
			url: 'http://localhost:8080/WeatherServer/RegisterServlet',
			data: JSON.stringify(info),
			success: function (data) {
				var loginInfo = $.parseJSON(data);
				sessionStorage.setItem("id", loginInfo.id);
				window.location.href = "main.html";
			},
			error: function () {
				window.location.href = "login.html";
			}
		})		
	}
});
$("#logOutButton").click(function() {
	sessionStorage.setItem("id", null);
});
function checkImg(type) {
	if (type == "Rain") {
		return "img/icons/shower-icon.svg";
	}
	if (type == "Clouds") {
		return "img/icons/cloudy-icon.svg";
	}
	if (type == "Clear") {
		return "img/icons/sunny-icon.svg";
	}
	if (type == "Snow") {
		return "img/icons/snowing-icon.svg";
	}
}
function getDate(date) {
	var newDate = date.split("");
	newDate[0] = date[3];
	newDate[1] = date[4];
	newDate[2] = date[2];
	newDate[3] = date[0];
	newDate[4] = date[1];
	for (var i = 5; i < newDate.length; i++) {
		newDate[i] = date[i];
	}
	var result = newDate.join("");
	var d = new Date(result)
	return result;
}
function getDayName(someDate) {
	var weekday = new Array(7);
	weekday[0]=  "Sunday";
	weekday[1] = "Monday";
	weekday[2] = "Tuesday";
	weekday[3] = "Wednesday";
	weekday[4] = "Thursday";
	weekday[5] = "Friday";
	weekday[6] = "Saturday";
	
	return weekday[someDate.getDay()];
}
$.ajax({
	type: 'GET',
	url: 'http://localhost:8080/WeatherServer/LoadServlet',
	success: function (data) {
		$('#city').text(data.name);
		$('#date').text(data.list[0].dt);
		$('#todayTemp').html(Math.floor(data.list[0].main.temp) + "&deg;");
		$('#todayHumidityValue').html(data.list[0].humidity + "%");
		$('#todayWindValue').html(data.list[0].wind + " m/s");
		$('#todayImg').attr("src", checkImg(data.list[0].description));
		$('#firstDayTemp').html(Math.floor(data.list[1].main.temp_min) + "&deg; | " + Math.floor(data.list[1].main.temp_max) + "&deg;");
		$('#firstDayImg').attr("src", checkImg(data.list[1].description));
		$('#firstDayName').text(getDayName(new Date(getDate(data.list[1].dt))));
		$('#secondDayTemp').html(Math.floor(data.list[2].main.temp_min) + "&deg; | " + Math.floor(data.list[2].main.temp_max) + "&deg;");
		$('#secondDayImg').attr("src", checkImg(data.list[2].description));
		$('#secondDayName').text(getDayName(new Date(getDate(data.list[2].dt))));
	},
	error: function () {
		window.location.href = "login.html";
	}
})		
})