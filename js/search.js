if(sessionStorage.getItem("id") == null || sessionStorage.getItem("id") == 'undefined') {
	window.location.replace("login.html");
};
$(function() {
	$("#threeDayTabToggle").click(function() {
		$("#threeDay").show();
		$("#sevenDay").hide();
	});
	$("#sevenDayTabToggle").click(function() {
		$("#sevenDay").show();
		$("#threeDay").hide();
	});
	
	$("#dropMenu, #dropButton").mouseover(function(){
         $("#dropButton").css("background-color", "rgba(37,102,149,0.7)");
     });
	$("#dropMenu, #dropButton").mouseout(function(){
         $("#dropButton").css("background", "rgba(0, 0, 0, 0.6)");
     });
	
	$("#threeDayTabToggle").click(function() {
		$('#threeDayTabToggle').removeClass("unselectedButton").addClass("selectedButton");
		$("#sevenDayTabToggle").removeClass("selectedButton").addClass("unselectedButton");
	});
	$("#sevenDayTabToggle").click(function() {
		$("#sevenDayTabToggle").removeClass("unselectedButton").addClass("selectedButton");
		$("#threeDayTabToggle").removeClass("selectedButton").addClass("unselectedButton");
	});
	$("#logOutButton").click(function() {
		sessionStorage.setItem("id", null);
	})
	function checkImg(type) {
		if (type == "Rain") {
			return "img/icons/shower-icon.svg";
		}
		if (type == "Clouds") {
			return "img/icons/cloudy-icon.svg";
		}
		if (type == "Clear") {
			return "img/icons/sunny-icon.svg";
		}
		if (type == "Snow") {
			return "img/icons/snowing-icon.svg";
		}
	}
	function getDate(date) {
		var newDate = date.split("");
		newDate[0] = date[3];
		newDate[1] = date[4];
		newDate[2] = date[2];
		newDate[3] = date[0];
		newDate[4] = date[1];
		for (var i = 5; i < newDate.length; i++) {
			newDate[i] = date[i];
		}
		var result = newDate.join("");
		var d = new Date(result)
		return result;
	}
	function getDayName(someDate) {
		var weekday = new Array(7);
		weekday[0]=  "Sunday";
		weekday[1] = "Monday";
		weekday[2] = "Tuesday";
		weekday[3] = "Wednesday";
		weekday[4] = "Thursday";
		weekday[5] = "Friday";
		weekday[6] = "Saturday";
		
		return weekday[someDate.getDay()];
	}
	$("#searchButton").click(function() {
		var info = {
			city: $("#cityInput").val(),
		}
		$.ajax({
			type: 'GET',
			url: 'http://localhost:8080/WeatherServer/ForecastServlet',
			data: info,
			success: function (data) {
				for(var i = 0; i < 4; i++) {
					$("#defaultWeatherHolder").hide();
					$("#weatherHolder").show();
					$("#location").html(data.name)
					$("#threeDay" + i).find(".dayValue").text(getDayName(new Date(getDate(data.list[i].dt))));
					$("#sevenDay" + i).find(".dayValue").text(getDayName(new Date(getDate(data.list[i].dt))));
					$("#threeDay" + i).find(".dateValue").text(data.list[i].dt);
					$("#sevenDay" + i).find(".dateValue").text(data.list[i].dt);
					$("#threeDay" + i).find(".imgValue").attr("src", checkImg(data.list[i].description));
					$("#sevenDay" + i).find(".imgValue").attr("src", checkImg(data.list[i].description));
					$("#threeDay" + i).find(".tempValue").html(Math.ceil(data.list[i].main.temp) + "&deg;");
					$("#sevenDay" + i).find(".tempValue").html(Math.ceil(data.list[i].main.temp_min) + "&deg; / " + Math.ceil(data.list[i].main.temp_max) + "&deg;");
					$("#threeDay" + i).find(".typeValue").text(data.list[i].description);
					$("#sevenDay" + i).find(".typeValue").text(data.list[i].description);
				}
				for (var i = 4; i < 7; i++){
					$("#sevenDay" + i).find(".dayValue").text(getDayName(new Date(getDate(data.list[i].dt))));
					$("#sevenDay" + i).find(".dateValue").text(data.list[i].dt);
					$("#sevenDay" + i).find(".imgValue").attr("src", checkImg(data.list[i].description));
					$("#sevenDay" + i).find(".tempValue").html(Math.ceil(data.list[i].main.temp_min) + "&deg; / " + Math.ceil(data.list[i].main.temp_max) + "&deg;");
					$("#sevenDay" + i).find(".typeValue").text(data.list[i].description);
				}
				
			},
			error: function () {
				window.location.href = "search.html";
			}
		})	
	})
})